//
//  TravelingSalesmanBuilder.cpp
//  agh
//
//  Created by Katelyn Dunn on 11/7/18.
//  Copyright © 2018 Katelyn Dunn. All rights reserved.
//

#include "TravelingSalesmanBuilder.hpp"

TravelingSalesmanBuilder::TravelingSalesmanBuilder(){
    
}
void TravelingSalesmanBuilder::build(int dataSize, string fileName){
    AdjacencyMatrix adjMat(dataSize);
    vector<Node> nodes;
    Node n;
    //string fileName = "/Users/katelyndunn/Desktop/Fall 2018/Fundamentals of Algorithms (CSE 3353)/Lab3/Lab3/positions13.txt" ; //
    string line;
    
    ifstream readFile;
    
    readFile.open(fileName);
    if (readFile.is_open())
    {
        char* charStar = new char[50];
        while (readFile.getline(charStar,50))
        {
            line = charStar;
            stringstream  ss(line);
            int nodeValue;
            ss >> nodeValue;
            n.val = nodeValue;
            nodes.push_back(n);
            char comma = 0;
            ss >> comma;
            float xVal = 0;
            ss >> xVal;
            ss >> comma;
            float yVal = 0;
            ss >> yVal;
            ss >> comma;
            float zVal = 0;
            ss >> zVal;
            
            nodes[nodeValue-1].positions.push_back(xVal);
            nodes[nodeValue-1].positions.push_back(yVal);
            nodes[nodeValue-1].positions.push_back(zVal);
            
            n.clear();
        }
        for(int i = 0; i < dataSize-1; i++) //
        {
            for(int k = 1; k < dataSize; k++) //
            {
                float distance = sqrt(pow((nodes[i].positions[0] -nodes[k].positions[0]), 2) + pow((nodes[i].positions[1] -nodes[k].positions[1]), 2) + pow((nodes[i].positions[2]-nodes[k].positions[2]), 2));
                
                adjMat.addEdge(nodes[i].val, nodes[k].val, distance);
                adjMat.addEdge(nodes[k].val, nodes[i].val, distance);
                
            }
            
        }
        
        adjMat.printAM();
    }
    
    NaiveTS nts;
    auto start_time = chrono::high_resolution_clock::now();
    cout << "NAIVE: "<<nts.travelingSalesman(adjMat, 0)<< endl;
    auto end_time = chrono::high_resolution_clock::now();
    double time= chrono::duration_cast<chrono::microseconds>(end_time - start_time).count();
    
    cout<< "TIME in MS:" << time ;
    
    DynamicTS dts;
    auto start_time1 = chrono::high_resolution_clock::now();
    set<int> visitedSet;
    visitedSet.insert(0);
    dts.createPaths(adjMat, 0, visitedSet,1, dataSize);
    auto end_time1 = chrono::high_resolution_clock::now();
    double time1= chrono::duration_cast<chrono::microseconds>(end_time1 - start_time1).count();
    cout << time1;
    
    
}
