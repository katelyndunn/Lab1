//
//  TravelingSalesmanBuilder.hpp
//  agh
//
//  Created by Katelyn Dunn on 11/7/18.
//  Copyright © 2018 Katelyn Dunn. All rights reserved.
//

#ifndef TravelingSalesmanBuilder_hpp
#define TravelingSalesmanBuilder_hpp

#include "NaiveTS.hpp"
#include "Node.hpp"
#include "DynamicTS.hpp"
#include "AdjacencyMatrix.hpp"

#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <cmath>
#include <string>
#include <chrono>

#include <stdio.h>

class TravelingSalesmanBuilder{
public:
    TravelingSalesmanBuilder();
    int size;
    //AdjacencyMatrix adjMat;
    void build(int dataSize, string fileName);
private:
    
};

#endif /* TravelingSalesmanBuilder_hpp */
