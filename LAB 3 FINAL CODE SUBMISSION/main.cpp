//
//  main.cpp
//  lab3again
//
//  Created by Katelyn Dunn on 11/6/18.
//  Copyright © 2018 Katelyn Dunn. All rights reserved.
//

#include "TravelingSalesmanBuilder.hpp"


int main(int argc, const char * argv[]) {
    int dataSetsize = 11;
    string fileName = "/Users/katelyndunn/Desktop/Fall 2018/Fundamentals of Algorithms (CSE 3353)/Lab3/Lab3/positions11.txt";
    TravelingSalesmanBuilder tsb;
    tsb.build(dataSetsize,fileName);
};
        
