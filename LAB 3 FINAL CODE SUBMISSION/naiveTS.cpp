//
//  NaiveTS.cpp
//  Lab3
//
//  Created by Katelyn Dunn on 10/25/18.
//  Copyright © 2018 Katelyn Dunn. All rights reserved.
// refereced: https://www.geeksforgeeks.org/traveling-salesman-problem-tsp-implementation/

#include "NaiveTS.hpp"

float NaiveTS::travelingSalesman(AdjacencyMatrix adjMat, int start)
{
     float minimumPath = FLT_MAX;
    
    for(int index = 0; index < adjMat.size(); index++)
    {
        if(index != start)
        {
            visited.push_back(index);
            //cout << index << endl;
        }
    }
    
    while (next_permutation(visited.begin(),visited.end()))
    {
        float currWeight = 0;
        vector<int> currPath;
        
        int k = start;
        for (int i = 0; i < visited.size(); i++) {
            currWeight += adjMat.get(k,visited[i]); //current path weight
            k = visited[i]; //incremenent "start" spot
            //cout << k;
        }
        currWeight += adjMat.get(k,start);
        currPath.push_back(k);
//        cout <<"START" <<start;
//        cout << "K" <<k; //back to the beginning
        
        minimumPath = min(minimumPath, currWeight);
    }
    return minimumPath;
}
