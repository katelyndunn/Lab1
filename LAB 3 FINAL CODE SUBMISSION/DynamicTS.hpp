//
//  DynamicTS.hpp
//  Lab3
//
//  Created by Katelyn Dunn on 10/31/18.
//  Copyright © 2018 Katelyn Dunn. All rights reserved.
//

#ifndef DynamicTS_hpp
#define DynamicTS_hpp

#include <stdio.h>

#include "AdjacencyMatrix.hpp"

#include <set>
#include <map>
#include <vector>
#include <iterator>
#include <algorithm>

using namespace std;

class DynamicTS{
private:
    //int visited[7];
    //set<int> nodes;
    vector<set<int>> visitedSets;
    
    
public:
    void travelingSalesMan(AdjacencyMatrix adjMat, int dataSetSize);
    void createPaths(AdjacencyMatrix adjMat,int start, set<int>, int, int dataSetSize);
};


#endif /* DynamicTS_hpp */

