//
//  NaiveTS.hpp
//  Lab3
//
//  Created by Katelyn Dunn on 10/25/18.
//  Copyright © 2018 Katelyn Dunn. All rights reserved.
//

#ifndef NaiveTS_hpp
#define NaiveTS_hpp

#include "AdjacencyMatrix.hpp"

#include <stdio.h>
#include <vector>
#include <array>
#include <iostream>
using namespace std;

class NaiveTS{
private:
    vector<int> visited;
public:
    int travelingSalesman(AdjacencyMatrix adj, int start);
};

#endif /* NaiveTS_hpp */
