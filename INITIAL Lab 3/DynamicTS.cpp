//
//  DynamicTS.cpp
//  Lab3
//
//  Created by Katelyn Dunn on 10/31/18.
//  Copyright © 2018 Katelyn Dunn. All rights reserved.
//

#include "DynamicTS.hpp"

void DynamicTS::travelingSalesMan(AdjacencyMatrix adjMat)
{
    for (int set = 0; set < visitedSets.size(); set++)
        {
            for(int possibleVal = 0; possibleVal < 4; possibleVal++) //curr
            {
               if( visitedSets[set].find(possibleVal) != visitedSets[set].end())
               {
                   continue;
               }
               else
               {
                   std::set<int>::iterator it;
                   for ( it = visitedSets[set].begin(); it != visitedSets[set].end(); ++it)
                   {
                       std::set<int> visitedSetTEMP = visitedSets[set];
                       int previousVal = *it;
                       visitedSetTEMP.erase(*it);
                       currentMin = min(currentMin,costMap[previousVal][visitedSetTEMP] + adjMat.get(previousVal,possibleVal));
                       cout << "CURRENT MIN: " << currentMin << endl;
                   }
                   costMap[possibleVal][visitedSets[set]] = currentMin;
                }
                
                cout << "POSSIBLE VAL: " << possibleVal << endl;
                std::set<int>::iterator it;
                for ( it = visitedSets[set].begin(); it != visitedSets[set].end(); ++it)
                {
                    if (it != visitedSets[set].end())
                    {
                       cout <<*it<<"-->";
                    }
                    else
                    {
                        cout << *it << endl;
                    }

                }
            }
            
        }
}

void DynamicTS::createPaths(AdjacencyMatrix adjMat, int startNode, set<int> VisitedSet, int combinations)
{
    for(int nextNode = startNode + 1; combinations < adjMat.size(); combinations++)
    {
        
        if(nextNode >= adjMat.size())
        {
            nextNode = 0;
        }
        std::set<int> tempVisitedSet = VisitedSet;
        tempVisitedSet.insert(nextNode);
        visitedSets.push_back(tempVisitedSet);
        createPaths(adjMat, nextNode, tempVisitedSet, combinations+1);
        nextNode++;
    }

}
