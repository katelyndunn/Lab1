//
//  main.cpp
//  Lab3
//
//  Created by Katelyn Dunn on 10/25/18.
//  Copyright © 2018 Katelyn Dunn. All rights reserved.
//

#include <iostream>
#include "NaiveTS.hpp"
#include "DynamicTS.hpp"
#include "AdjacencyMatrix.hpp"
#include <vector>
#include <string>

void permute (char* cities, int l, int r);
void swap(char *x, char *y);

int main(int argc, const char * argv[]) {

    AdjacencyMatrix adjMat(4);
   
    adjMat.addEdge(1,2,4);
    adjMat.addEdge(1,3,7);
    adjMat.addEdge(1,4,3);
    adjMat.addEdge(2,1,4);
    adjMat.addEdge(2,3,2);
    adjMat.addEdge(2,4,9);
    adjMat.addEdge(3,1,7);
    adjMat.addEdge(3,2,2);
    adjMat.addEdge(3,4,5);
    adjMat.addEdge(4,1,3);
    adjMat.addEdge(4,3,5);
    adjMat.addEdge(4,2,9);
    
    adjMat.printAM();
    
    NaiveTS nts;
    cout << "NAIVE: "<<nts.travelingSalesman(adjMat, 0)<< endl;
    
    DynamicTS dts;
    std::set<int> visitedSet{0};
    //visitedSet.insert(2);
    dts.createPaths(adjMat,0,visitedSet,1);
    dts.travelingSalesMan(adjMat);
//    dts.minimumCost(adjMat,0);
//    cout << endl <<"DYNAMIC: "<<dts.cost;

    }

