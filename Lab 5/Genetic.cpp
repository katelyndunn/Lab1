//
//  Genetic.cpp
//  lastlab
//
//  Created by Katelyn Dunn on 12/12/18.
//  Copyright © 2018 Katelyn Dunn. All rights reserved.
//

#include "Genetic.hpp"
vector<float> Genetic::search(AdjacencyMatrix adjMat, int numberOfIterations, int size, int startNode){
    setSize = size;
    populationSize = 5;
    for(int i = 0; i < populationSize; i++)
    {
        //int initialNodeSize = 10;
        int startNode = 1;
        Neighborhood n;
        for(int k = 1; k <= setSize; k++ )
        {
            n.path.push_back(k);
        }
        n.path.push_back(startNode);
        
        int shuffleSize =  (rand() % setSize + 1);
        for( int k = 0; k < shuffleSize; k++)
        {
            std::random_shuffle(n.path.begin()+1, n.path.end()-1);
            
        }
        cout << "ORIGINAL PATHS: ";
        for(int i = 0; i < n.path.size(); i++)
        {
            cout << n.path[i]<<" ";
        }
        
        calculatePathCost(n, adjMat);
        cout << "COST: "<<n.distCost<<endl;
        neighbors.push_back(n);
        
        
    }
    
    for(int z = 0; z < numberOfIterations; z++)
    {
        sortByCost();
        
        //SELECTION-----------------
        //ELITEST
        //eliteSelection();
        //RANDOM
        randomSelection();
        
        neighbors.clear();
        //CROSSOVER-----------------
        //single point cross over
        crossOver1(adjMat);
        //two random point cross over
        //twoPointCrossOver(adjMat);
        
        
        //MUTATION-------------------
        //Scramble Mutation
        //scrambleMutation(adjMat);
        //Swap Mutation
        mutationSwaps(adjMat);
        
        
    }
    sortByCost();
    cout << "GENETIC BEST PATH: "<< endl;
    for(int i = 0; i < orderedNeighbors[0].path.size(); i++)
    {
        cout << orderedNeighbors[0].path[i]<<" ";
    }
    cout << "BEST PATH COST:"<<orderedNeighbors[0].distCost << endl;
    
    return  orderedNeighbors[0].path;
}

//FUNCTION VECTOR

void Genetic::calculatePathCost(Neighborhood& n, AdjacencyMatrix adjMat){
    for(int i = 0; i < n.path.size()-1; i++)
    {
        //cout << n.distCost << " + "<< adjMat.get(i,i+1) <<endl;
        n.distCost += adjMat.get(n.path[i],n.path[i+1]);
        
    }
    
    //cout << endl << endl;
}
void Genetic::sortByCost()
{
    orderedNeighbors.clear();
    vector<float> tempVector;
    for(int i = 0; i < neighbors.size(); i++)
    {
        tempVector.push_back(neighbors[i].distCost);
    }
    sort(tempVector.begin(), tempVector.end());
    
    //select top
    for(int i = 0; i < tempVector.size(); i++) //FIX THIS
    {
        for(int j = 0; j < neighbors.size(); j++)
        {
            if(tempVector[i] == neighbors[j].distCost)
            {
                orderedNeighbors.push_back(neighbors[j]);
            }
        }
    }
}

void Genetic::eliteSelection()
{
    parents.clear();
    
    for(int i = 0; i< populationSize; i++)
    {
        parents.push_back(orderedNeighbors[i]);
    }
}


void Genetic::randomSelection()
{
    parents.clear();
    if(neighbors.size() == populationSize)
    {
        for(int i = 0; i< populationSize; i++)
        {
            parents.push_back(neighbors[i]);
        }
    }
    else
    {
        for(int i = 0; i < populationSize; i++)
        {
            int chosen =  (rand() % (populationSize-1)+1);
            parents.push_back(neighbors[chosen]);
            
        }
    }
}

void Genetic::crossOver1(AdjacencyMatrix adjMat){
    for(int i = 0; i < parents.size()-1; i++)
    {
        //FRONT TO BACK
        Neighborhood tempNeighbor;
        int pathSize = parents[i].path.size();
        for(int j = 0; j < parents[i].path.size()/2; j++ )
        {
            tempNeighbor.path.push_back(parents[i].path[j]);
            //cout << parents[i].path[j] << " ";
        }
        
        for(int k = 0; k < parents[i+1].path.size(); k++) //figure out how to get it filled in
        {
            std::vector<float>::iterator it;
            
            it = find (tempNeighbor.path.begin(), tempNeighbor.path.end(), parents[i+1].path[k]);
            
            if (it != tempNeighbor.path.end())
            {
                continue;
            }
            else
            {
                tempNeighbor.path.push_back(parents[i+1].path[k]);
                //cout << parents[i+1].path[k] << " ";
            }
        }
        tempNeighbor.path.push_back(tempNeighbor.path[0]);
        calculatePathCost(tempNeighbor, adjMat);
        neighbors.push_back(tempNeighbor);
        
    }
    
    //BACK TO FRONT
    for(int i = parents.size()-1; i > 0; i--)
    {
        Neighborhood tempNeighbor;
        int pathSize = parents[i].path.size();
        for(int j = 0; j < parents[i].path.size()/2; j++ )
        {
            tempNeighbor.path.push_back(parents[i].path[j]);
            //cout << parents[i].path[j] << " ";
        }
        ///
        int z;
        for(int k = 0; k < parents[i-1].path.size(); k++) //figure out how to get it filled in
        {
            std::vector<float>::iterator it;
            
            it = find (tempNeighbor.path.begin(), tempNeighbor.path.end(), parents[i-1].path[k]);
            
            if (it != tempNeighbor.path.end())
            {
                continue;
            }
            else
            {
                tempNeighbor.path.push_back(parents[i-1].path[k]);
                //cout << parents[i+1].path[k] << " ";
            }
        }
        tempNeighbor.path.push_back(tempNeighbor.path[0]);
        calculatePathCost(tempNeighbor, adjMat);
        neighbors.push_back(tempNeighbor);
        
    }
}

void Genetic::twoPointCrossOver(AdjacencyMatrix adjMat)
{
    //FRONT TO BACK
    for(int i = 0; i < parents.size()-1; i++)
    {
        int crossOverStart =  (rand() % setSize + 2); //change according to data size
        int crossOverEnd = (rand() % setSize + 2);
        if(crossOverStart == crossOverEnd)
        {
            crossOverEnd = (rand() % setSize + 2);
        }
        Neighborhood tempNeighbor;
        tempNeighbor.path.push_back(parents[i].path[0]);
        for(int j = crossOverStart; j < crossOverEnd; j++ )
        {
            tempNeighbor.path.push_back(parents[i].path[j]);
            //cout << parents[i].path[j] << " ";
        }
        ///
        for(int k = 0; k < parents[i+1].path.size(); k++)
        {
            std::vector<float>::iterator it;
            
            it = find (tempNeighbor.path.begin(), tempNeighbor.path.end(), parents[i+1].path[k]);
            
            if (it != tempNeighbor.path.end())
            {
                continue;
            }
            else
            {
                tempNeighbor.path.push_back(parents[i+1].path[k]);
                //cout << parents[i+1].path[k] << " ";
            }
        }
        tempNeighbor.path.push_back(tempNeighbor.path[0]);
        calculatePathCost(tempNeighbor, adjMat);
        neighbors.push_back(tempNeighbor);
        
    }
    
    //BACK TO FRONT
    for(int i = parents.size()-1; i > 0; i--)
    {
        int crossOverStart =  (rand() % setSize + 2); //change according to data setSize
        int crossOverEnd = (rand() % setSize + 1);
        if(crossOverStart == crossOverEnd)
        {
            crossOverEnd = (rand() % setSize + 1);
        }
        Neighborhood tempNeighbor;
        tempNeighbor.path.push_back(parents[i].path[0]);
        for(int j = crossOverStart; j < crossOverEnd; j++ )
        {
            tempNeighbor.path.push_back(parents[i].path[j]);
            //cout << parents[i].path[j] << " ";
        }
        ///
        for(int k = 0; k < parents[i-1].path.size(); k++)
        {
            std::vector<float>::iterator it;
            
            it = find (tempNeighbor.path.begin(), tempNeighbor.path.end(), parents[i-1].path[k]);
            
            if (it != tempNeighbor.path.end())
            {
                continue;
            }
            else
            {
                tempNeighbor.path.push_back(parents[i-1].path[k]);
                //cout << parents[i+1].path[k] << " ";
            }
        }
        tempNeighbor.path.push_back(tempNeighbor.path[0]);
        calculatePathCost(tempNeighbor, adjMat);
        neighbors.push_back(tempNeighbor);
        
    }
}

void Genetic::mutationSwaps(AdjacencyMatrix adjMat)
{
    //cout<<endl<<"PARENT SIZE:"<<parents.size()<<endl;
    for(int i = 0; i < parents.size()-1; i++){
        for(int j = 1; j < parents[i].path.size()-2; j++)
        {
            for(int k = j + 1; k < parents[i].path.size()-1; k++)
            {
                Neighborhood tempNeighbor;
                tempNeighbor = parents[i];
                tempNeighbor.distCost = 0;
                
                float tempVal;
                tempVal = tempNeighbor.path[j];
                tempNeighbor.path[j]= tempNeighbor.path[k];
                tempNeighbor.path[k] = tempVal;
                
                calculatePathCost(tempNeighbor, adjMat);
                //cout << "PARENT:" << i << endl;
                neighbors.push_back(tempNeighbor);
                
            }
        }
    }
}

void Genetic::scrambleMutation(AdjacencyMatrix adjMat){
    
    for( int k = 0; k < parents.size(); k++){
        Neighborhood n;
        n.path = parents[k].path;
        std::random_shuffle(n.path.begin()+3, n.path.end()-3);
        calculatePathCost(n, adjMat);
        neighbors.push_back(n);
        
    }
    
}
