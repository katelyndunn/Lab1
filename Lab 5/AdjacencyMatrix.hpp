//
//  AdjacencyMatrix.hpp
//  lastlab
//
//  Created by Katelyn Dunn on 12/12/18.
//  Copyright © 2018 Katelyn Dunn. All rights reserved.
//based off of https://www.sanfoundry.com/cpp-program-implement-adjacency-matrix/

#ifndef AdjacencyMatrix_hpp
#define AdjacencyMatrix_hpp

#include <stdio.h>

#include <iostream>
using namespace std;

class AdjacencyMatrix{
private:
    int n;
    float ** adj;
    bool* visited;
    
public:
    AdjacencyMatrix(int n);
    void addEdge(int start, int end, float weight);
    void printAM();
    int get(int start, int end);
    int size();

};



#endif /* AdjacencyMatrix_hpp */
