//
//  Tabu.hpp
//  lastlab
//
//  Created by Katelyn Dunn on 12/12/18.
//  Copyright © 2018 Katelyn Dunn. All rights reserved.
//

#ifndef Tabu_hpp
#define Tabu_hpp

#include <stdio.h>
#include "Algorithm.hpp"

#include <algorithm>
#include <map>
#include <set>
#include <vector>

class Tabu: public Algorithm{
public:
    vector<float> search(AdjacencyMatrix, int,int,int);
private:
    map<set<float>,int> tabuList;
    vector<float> bestPath;
    vector<Neighborhood> neighbors;
    float bestCost;
    float currMinCost;
    vector<float> currMinPath;
    set<float> currSwap;
    Neighborhood finalSolution1;
    Neighborhood finalSolution2;
    
    
    void calculatePathCost(Neighborhood&, AdjacencyMatrix);
    Neighborhood& neighborSelection1(Neighborhood&, AdjacencyMatrix&);
    Neighborhood& neighborSelection2(Neighborhood&, AdjacencyMatrix&);
    
};

#endif /* Tabu_hpp */
