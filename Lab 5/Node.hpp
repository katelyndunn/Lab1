//
//  Node.hpp
//  lastlab
//
//  Created by Katelyn Dunn on 12/12/18.
//  Copyright © 2018 Katelyn Dunn. All rights reserved.
//

#ifndef Node_hpp
#define Node_hpp

#include <stdio.h>
#include <vector>
using namespace std;

class Node {
public:
    int val;
    vector<float> positions;
    void clear();
private:
    
};
#endif /* Node_hpp */
