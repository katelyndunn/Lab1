//
//  Neighborhood.hpp
//  lastlab
//
//  Created by Katelyn Dunn on 12/12/18.
//  Copyright © 2018 Katelyn Dunn. All rights reserved.
//

#ifndef Neighborhood_hpp
#define Neighborhood_hpp

#include <stdio.h>
#include <utility>
#include <vector>

using namespace std;

class Neighborhood {
public:
    Neighborhood();
    ~Neighborhood();
    Neighborhood(const Neighborhood&);
    Neighborhood& operator=( const Neighborhood& n );
    vector<float> path; //neighborhood
    float distCost; //DISTANCE only cost
    int penalty; //growing penalty
    int swap; //what to put in tabu list
    float personalBestCost;
    vector<pair<int,int>> velocity;
    vector<float> personalBest;
private:
    
};

#endif /* Neighborhood_hpp */
