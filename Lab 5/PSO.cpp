//
//  PSO.cpp
//  lastlab
//
//  Created by Katelyn Dunn on 12/12/18.
//  Copyright © 2018 Katelyn Dunn. All rights reserved.
//

#include "PSO.hpp"
//BASED OFF OF:
//"http://www.cs.mun.ca/~tinayu/Teaching_files/cs4752/Lecture19_new.pdf"

vector<float> PSO::search(AdjacencyMatrix adjMat, int numberOfIterations, int size, int start)
{
    //INTIAL SET----------------------------
    for(int j = 0; j < size; j++)
    {
        int startNode = 1;
        Neighborhood n;
        for(int k = 1; k <= size; k++ )
        {
            n.path.push_back(k);
        }
        n.path.push_back(startNode);
        
        std::random_shuffle(n.path.begin()+1, n.path.end()-1);
        
        calculatePathCost(n, adjMat);
        n.personalBest = n.path;
        n.personalBestCost = n.distCost;
        initialVelocity(n);
        particles.push_back(n);
        if(particles.size() == 1){
            globalBest = n.path;
            globalBestCost = n.distCost;
        }
        else if (n.distCost < globalBestCost)
        {
            globalBest = n.path;
            globalBestCost = n.distCost;
        }
        cout << "INITIAL SET #"<<j<<":"<<endl;
        for(int i = 0; i < particles[j].path.size(); i++)
        {
            cout << particles[j].path[i]<<" ";
        }
        cout << "COST:"<< particles[j].distCost << endl<<endl;
        
    }
    //--------------------------------------
    
    //THE REST------------------------------
    for(int i = 0; i < numberOfIterations; i++)
    {
        for(int k = 0 ; k < particles.size(); k++)
        {
            calculateVelocity(particles[k]);
            executeVelocity(particles[k]);
            calculatePathCost(particles[k], adjMat);
            //set new personal best
            if(particles[k].distCost < particles[k].personalBestCost)
            {
                particles[k].personalBest = particles[k].path;
                particles[k].personalBestCost = particles[k].distCost;
                cout << "PERSONAL BEST: "<<endl;
                for(int z =0; z < particles[k].personalBest.size(); z++)
                {
                    cout << particles[k].personalBest[z]<<" ";
                }
                cout << "PERSONAL BEST COST: "<<particles[k].personalBestCost<<endl;
            }
            setNewGBest();
        }
        
        
    }
    cout << "GLOBAL BEST: "<<endl;
    for(int z =0; z < globalBest.size(); z++)
    {
        cout << globalBest[z]<<" ";
    }
    cout << "GLOBAL BEST COST: "<< globalBestCost <<endl;
    
    return globalBest;
    
}

void PSO::initialVelocity(Neighborhood& n)
{
    int min = n.path[0];
    int range = (n.path.size()-1) - min;
    
    float swap1 =  rand() % range + min;
    float swap2 =  rand() % range + min;
    
    while (swap1 == swap2)
    {
        swap2 =  rand() % range + min;;
    }
    
    pair<int,int> iSwaps;
    iSwaps = make_pair(swap1,swap2);
    
    n.velocity.push_back(iSwaps);
    
}

void PSO::calculatePathCost(Neighborhood& n, AdjacencyMatrix adjMat){
    n.distCost = 0;
    for(int i = 0; i < n.path.size()-1; i++)
    {
        n.distCost += adjMat.get(n.path[i],n.path[i+1]);
    }
    
}

void PSO::setNewGBest() //IS THIS BASED OFF OF PERSONAL BEST OR JUST CURRENT ITERATION?
{
    vector<Neighborhood> orderedParticles;
    vector<float> tempVector;
    for(int i = 0; i < particles.size(); i++)
    {
        tempVector.push_back(particles[i].personalBestCost);
    }
    sort(tempVector.begin(), tempVector.end());
    
    for(int i = 0; i < tempVector.size(); i++)
    {
        for(int j = 0; j < particles.size(); j++)
        {
            if(tempVector[i] == particles[j].personalBestCost)
            {
                orderedParticles.push_back(particles[j]);
            }
        }
    }
    
    if( orderedParticles[0].personalBestCost < globalBestCost )
    {
        globalBest = orderedParticles[0].personalBest;
        globalBestCost = orderedParticles[0].personalBestCost ;
        cout << "NEW GLOBAL: "<< globalBestCost;
        
    }
}

void PSO::calculateVelocity(Neighborhood& n)
{
    vector<pair<int,int>> newVelocity;
    for(int i = 0; i < n.velocity.size(); i++)
    {
        newVelocity.push_back(n.velocity[i]);
    }
    float randomVal1 = (rand() / double(RAND_MAX));
    if(randomVal1 > .5 )
    {
        vector<pair<int,int>> velocityToGlobal = velocityTo(n,globalBest);
        for(int i = 0; i < velocityToGlobal.size(); i++)
        {
            newVelocity.push_back(velocityToGlobal[i]);
        }
    }
    float randomVal2 = (rand() / double(RAND_MAX));
    if(randomVal2 > .5) //does this need to happen twice?
    {
        vector<pair<int,int>> velocityToPersonal = velocityTo(n,n.personalBest);
        for(int i = 0; i < velocityToPersonal.size(); i++)
        {
            
            newVelocity.push_back(velocityToPersonal[i]);
        }
    }
    
    n.velocity = newVelocity;
    if(n.velocity.size() > 10)
    {
        n.velocity.erase(n.velocity.begin());
    }
}

vector<pair<int,int>> PSO::velocityTo(Neighborhood& n,vector<float> best)
{
    vector<pair<int,int>> velocityTo;
    for(int i = 0; i < best.size(); i++)
    {
        if(n.path[i]==best[i])
        {
            continue;
        }
        else
        {
            for(int k = i+1; k < n.path.size(); k++)
            {
                if(best[i] == n.path[k])
                {
                    pair<int,int> swap;
                    swap = make_pair(i, k);
                    velocityTo.push_back(swap);
                    break;
                }
                else{
                    continue;
                }
            }
        }
    }
    
    return velocityTo;
}

void PSO::executeVelocity(Neighborhood& n)
{
    for(int i = 0; i < n.velocity.size(); i++)
    {
        float temp;
        int begin = n.velocity[i].first;
        int end = n.velocity[i].second;
        temp = n.path[begin];
        n.path[begin]=n.path[end];
        n.path[end] = temp;
        //n.penalty += 1;
    }
    
    
}
