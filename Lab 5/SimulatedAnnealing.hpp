//
//  SimulatedAnnealing.hpp
//  lastlab
//
//  Created by Katelyn Dunn on 12/12/18.
//  Copyright © 2018 Katelyn Dunn. All rights reserved.
//

#ifndef SimulatedAnnealing_hpp
#define SimulatedAnnealing_hpp

#include <stdio.h>
#include "Algorithm.hpp"

#include <algorithm>
#include <map>
#include <utility>
#include <vector>
#include <math.h>


#include <stdio.h>

using namespace std;

class SimulatedAnnealing: public Algorithm{
public:
    vector<float> search(AdjacencyMatrix, int,int,int);
private:
    float t = 1.0;
    float tMin = 0.00001;
    float alpha = 0.9;
    
    Neighborhood n;
    
    void calculatePathCost(Neighborhood&, AdjacencyMatrix);
    Neighborhood& neighborSelection(Neighborhood&, AdjacencyMatrix&);
    float acceptanceProbability(float, float);
};

#endif /* SimulatedAnnealing_hpp */
