//
//  Algorithm.hpp
//  lastlab
//
//  Created by Katelyn Dunn on 12/12/18.
//  Copyright © 2018 Katelyn Dunn. All rights reserved.
//

#ifndef Algorithm_hpp
#define Algorithm_hpp

#include <stdio.h>
#include "AdjacencyMatrix.hpp"
#include "Neighborhood.hpp"
#include "Node.hpp"
#include <iostream>
#include <fstream>
#include <sstream>
#include <math.h>

class Algorithm{
public:
    AdjacencyMatrix load(string, int);
    virtual vector<float>  search(AdjacencyMatrix adjMat, int numberOfIterations, int size, int startAt) = 0;
    virtual void  calculatePathCost(Neighborhood&, AdjacencyMatrix) = 0;
private:
};
#endif /* Algorithm_hpp */
