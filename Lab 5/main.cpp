//
//  main.cpp
//  lastlab
//
//  Created by Katelyn Dunn on 12/12/18.
//  Copyright © 2018 Katelyn Dunn. All rights reserved.
//

#include <iostream>
#include "AdjacencyMatrix.hpp"
#include "Tabu.hpp"
#include "Genetic.hpp"
#include "simulatedAnnealing.hpp"
#include "PSO.hpp"
#include <ctime>
#include <ratio>
#include <chrono>

using namespace std::chrono;


int main(int argc, const char * argv[]) {
    //Tabu tabu;
    int size = 10;
    int startPoint = 1;
    int numberOfIterations = 1000;
    string file;
    file = "‎⁨‎⁨Users/katelyndunn/Desktop/Fall 2018/Fundamentals of Algorithms (CSE 3353)/Lab3/Lab3/positions11.txt";
    //------------------------------------------
    //    high_resolution_clock::time_point ga1 = high_resolution_clock::now();
    //    Genetic genetic;
    //    genetic.search(genetic.load(file,size), numberOfIterations,size,startPoint);
    //    high_resolution_clock::time_point ga2 = high_resolution_clock::now();
    //
    //    duration<double> geneticAlgorithmTime = duration_cast<duration<double>>(ga2 - ga1);
    //
    //    cout << "GENETIC ALGORITHM TIME: "<<geneticAlgorithmTime.count() << endl;
    //
    //--------------------------------------------
//    high_resolution_clock::time_point ta1 = high_resolution_clock::now();
//    Tabu tabu;
//    tabu.search(tabu.load(file,size), numberOfIterations, size,startPoint);
//    high_resolution_clock::time_point ta2 = high_resolution_clock::now();
//    duration<double> tabuAlgorithmTime = duration_cast<duration<double>>(ta2 - ta1);
//    cout << "TABU ALGORITHM TIME: "<<tabuAlgorithmTime.count() << endl;
//
    //----------------------------------------------
//    high_resolution_clock::time_point sa1 = high_resolution_clock::now();
//    SimulatedAnnealing sA;
//    sA.search(sA.load(file,size), numberOfIterations, size, startPoint);
//    high_resolution_clock::time_point sa2 = high_resolution_clock::now();
//    duration<double> sATime = duration_cast<duration<double>>(sa2 - sa1);
//    cout << "SA ALGORITHM TIME: "<<sATime.count() << endl;
    
    //-----------------------------------------------
        high_resolution_clock::time_point pso1 = high_resolution_clock::now();
        PSO pso;
        pso.search(pso.load(file, size), numberOfIterations, size, startPoint);
        high_resolution_clock::time_point pso2 = high_resolution_clock::now();
        duration<double> psoTime = duration_cast<duration<double>>(pso2 - pso1);
        cout << "PSO ALGORITHM TIME: "<< psoTime.count() << endl;
}
