//
//  Genetic.hpp
//  lastlab
//
//  Created by Katelyn Dunn on 12/12/18.
//  Copyright © 2018 Katelyn Dunn. All rights reserved.
//

#ifndef Genetic_hpp
#define Genetic_hpp

#include <stdio.h>
#include "Algorithm.hpp"

#include <stdlib.h>
#include <algorithm>
#include <map>

class Genetic{
public:
    vector<float> search(AdjacencyMatrix, int, int, int);
private:
    int populationSize;
    int setSize;
    
    vector<Neighborhood> neighbors;
    vector<Neighborhood> orderedNeighbors;
    vector<Neighborhood> parents;
    vector<Neighborhood> fittest;
    
    vector<float> bestPath;
    vector<float> currMinPath;
    
    void calculatePathCost(Neighborhood&, AdjacencyMatrix);
    
    void sortByCost();
    
    //selection
    void eliteSelection(); //1
    void randomSelection(); //2
    
    //crossover
    void crossOver1(AdjacencyMatrix); //1
    void twoPointCrossOver(AdjacencyMatrix); //2
    
    //mutation
    void mutationSwaps(AdjacencyMatrix); //1
    void scrambleMutation(AdjacencyMatrix); //2
};

#endif /* Genetic_hpp */
