//
//  Algorithm.cpp
//  lastlab
//
//  Created by Katelyn Dunn on 12/12/18.
//  Copyright © 2018 Katelyn Dunn. All rights reserved.
//

#include "Algorithm.hpp"

AdjacencyMatrix Algorithm::load(string fileName, int dataSize){
    AdjacencyMatrix adjMat(dataSize);
    vector<Node> nodes;
    Node n;
    
    string line;
    
    ifstream readFile;
    
    readFile.open("/Users/katelyndunn/Desktop/Fall 2018/Fundamentals of Algorithms (CSE 3353)/lastlab/lastlab/positions.txt");
    if (readFile.is_open())
    {
        char* charStar = new char[50];
        while (readFile.getline(charStar,50))
        {
            line = charStar;
            stringstream  ss(line);
            int nodeValue;
            ss >> nodeValue;
            n.val = nodeValue;
            nodes.push_back(n);
            char comma = 0;
            ss >> comma;
            float xVal = 0;
            ss >> xVal;
            ss >> comma;
            float yVal = 0;
            ss >> yVal;
            ss >> comma;
            float zVal = 0;
            ss >> zVal;
            
            nodes[nodeValue-1].positions.push_back(xVal);
            nodes[nodeValue-1].positions.push_back(yVal);
            nodes[nodeValue-1].positions.push_back(zVal);
            
            n.clear();
        }
        for(int i = 0; i < dataSize-1; i++) //
        {
            for(int k = 1; k < dataSize; k++) //
            {
                float distance = sqrt(pow((nodes[i].positions[0] -nodes[k].positions[0]), 2) + pow((nodes[i].positions[1] -nodes[k].positions[1]), 2) + pow((nodes[i].positions[2]-nodes[k].positions[2]), 2));
                
                adjMat.addEdge(nodes[i].val, nodes[k].val, distance);
                adjMat.addEdge(nodes[k].val, nodes[i].val, distance);
                
            }
            
        }
        
        adjMat.printAM();
    }
    else{
        cout << "Cant read file";
    }
    
    return adjMat;
}
