//
//  Neighborhood.cpp
//  lastlab
//
//  Created by Katelyn Dunn on 12/12/18.
//  Copyright © 2018 Katelyn Dunn. All rights reserved.
//

#include "Neighborhood.hpp"

Neighborhood::Neighborhood(){
    distCost = 0; //DISTANCE only cost
    penalty = 0; //growing penalty
    swap = 0;
    personalBestCost = 0;
}

Neighborhood::~Neighborhood(){
    
}

Neighborhood::Neighborhood(const Neighborhood &n){
    distCost = n.distCost;
    penalty = n.penalty;
    swap = n.swap;
    personalBestCost = n.personalBestCost;
    for (int i = 0; i < n.path.size(); i++)
    {
        path.push_back(n.path[i]);
        //personalBest.push_back(n.personalBest[i]);
    }
    //velocity.push_back(make_pair(n.velocity[0].first, n.velocity[0].second));
}

Neighborhood& Neighborhood::operator=( const Neighborhood& n ){
    if(this != &n)
    {
        distCost = n.distCost;
        penalty = n.penalty;
        swap = n.swap;
        personalBestCost = n.personalBestCost;
        path.clear();
        for (int i = 0; i < n.path.size(); i++)
        {
            path.push_back(n.path[i]);
            personalBest.push_back(n.personalBest[i]);
        }
    }
    return *this;
}



