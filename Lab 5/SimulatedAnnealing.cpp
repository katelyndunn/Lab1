//
//  SimulatedAnnealing.cpp
//  lastlab
//
//  Created by Katelyn Dunn on 12/12/18.
//  Copyright © 2018 Katelyn Dunn. All rights reserved.
//

#include "SimulatedAnnealing.hpp"
vector<float> SimulatedAnnealing::search(AdjacencyMatrix adjMat, int numberOfIterations, int size, int start){
    //INITAL SOLUTION------------------------------------
    Neighborhood old;
    for(int i = 1; i <= size; i++ )
    {
        old.path.push_back(i);
    }
    old.path.push_back(start);
    
    calculatePathCost(old, adjMat);
    
    
    cout << "START PATH: ";
    for (int i = 0; i < old.path.size(); i++)
    {
        cout << old.path[i]<<" ";
    }
    cout << old.distCost<<endl;
    
    float acceptValue;
    //tabuList.clear();
    
    
    while( t > tMin)
    {
        for(int i = 0; i < numberOfIterations; i++)
        {
            Neighborhood curr = neighborSelection(old, adjMat);
            acceptValue = acceptanceProbability(old.distCost, curr.distCost);
            float randomVal = (rand() / double(RAND_MAX));
            if(acceptValue > randomVal){
                old = curr;
            }
            cout << "CURRENT PATH: ";
            for (int i = 0; i < curr.path.size(); i++)
            {
                cout << curr.path[i]<<" ";
            }
            cout <<endl <<curr.distCost<< endl;
        }
        t = t*alpha; //is this in right spot??
    }
    
    cout << "FINAL PATH: ";
    for(int s = 0; s < old.path.size(); s++)
    {
        cout<<old.path[s] << " ";
    }
    cout <<endl<<"COST: "<<old.distCost << endl;
    return old.path;
}

Neighborhood& SimulatedAnnealing::neighborSelection(Neighborhood& old, AdjacencyMatrix& adjMat){
    
    n = old;
    n.distCost = 0;
    
    int min = old.path[0];
    int range = (old.path.size()-1) - min;
    
    float swap1 =  rand() % range + min;
    float swap2 =  rand() % range + min;
    
    if (swap1 == swap2)
    {
        swap2 =  rand() % range + min;;
    }
    float temp;
    temp = n.path[swap1];
    n.path[swap1]=n.path[swap2];
    n.path[swap2] = temp;
    
    calculatePathCost(n, adjMat);
    //cout << n.distCost << endl;
    
    return n;
}

float SimulatedAnnealing::acceptanceProbability(float oldCost, float currCost)
{
    float aP;
    aP = exp(-(currCost-oldCost)/t);
    cout << aP;
    
    return aP;
}

void SimulatedAnnealing::calculatePathCost(Neighborhood& n, AdjacencyMatrix adjMat){
    for(int i = 0; i < n.path.size()-1; i++)
    {
        n.distCost += adjMat.get(n.path[i],n.path[i+1]);
    }
    
}
