//
//  PSO.hpp
//  lastlab
//
//  Created by Katelyn Dunn on 12/12/18.
//  Copyright © 2018 Katelyn Dunn. All rights reserved.
//

#ifndef PSO_hpp
#define PSO_hpp

#include <stdio.h>
#include "Algorithm.hpp"

#include <algorithm>
#include <map>
#include <utility>
#include <vector>

using namespace std;
class PSO: public Algorithm{
public:
    vector<float> search(AdjacencyMatrix, int,int,int);
private:
    
    void initialVelocity(Neighborhood&);
    void calculatePathCost(Neighborhood&, AdjacencyMatrix);
    
    void setNewGBest();
    void calculateVelocity(Neighborhood&);
    void executeVelocity(Neighborhood&);
    
    vector<pair<int,int>> velocityTo(Neighborhood&, vector<float>);
    
    vector<Neighborhood> particles;
    
    vector<float> globalBest;
    float globalBestCost;
};


#endif /* PSO_hpp */
