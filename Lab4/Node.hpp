//
//  Node.hpp
//  lab3again
//
//  Created by Katelyn Dunn on 11/6/18.
//  Copyright © 2018 Katelyn Dunn. All rights reserved.
//

#ifndef Node_hpp
#define Node_hpp
#include <vector>

using namespace std;

#include <stdio.h>
class Node{
public:
    int val;
    vector<float> positions;
    void clear();
private:
    
};
#endif /* Node_hpp */
