//
//  main.cpp
//  Lab4
//
//  Created by Katelyn Dunn on 11/17/18.
//  Copyright © 2018 Katelyn Dunn. All rights reserved.
//

#include <iostream>
#include "AdjacencyMatrix.hpp"
#include "Tabu.hpp"
#include "Genetic.hpp"
#include <ctime>
#include <ratio>
#include <chrono>

using namespace std::chrono;

int main(int argc, const char * argv[]) {
    //Tabu tabu;
    int size = 11;
    int startPoint = 1;
    int numberOfIterations = 100;
    string file;
    file = "‎⁨‎⁨/Users/katelyndunn/Desktop/Fall 2018/Fundamentals of Algorithms (CSE 3353)/Lab4/Lab4/positions.txt";
    //------------------------------------------
    high_resolution_clock::time_point ga1 = high_resolution_clock::now();
    Genetic genetic;
    genetic.search(genetic.load(file,size), numberOfIterations,size,startPoint);
    high_resolution_clock::time_point ga2 = high_resolution_clock::now();

    duration<double> geneticAlgorithmTime = duration_cast<duration<double>>(ga2 - ga1);

    cout << "GENETIC ALGORITHM TIME: "<<geneticAlgorithmTime.count() << endl;

    
    //--------------------------------------------
//     high_resolution_clock::time_point ta1 = high_resolution_clock::now();
//    Tabu tabu;
//    tabu.search(tabu.load(file,size), numberOfIterations, size,startPoint);
//     high_resolution_clock::time_point ta2 = high_resolution_clock::now();
//
//    duration<double> tabuAlgorithmTime = duration_cast<duration<double>>(ta2 - ta1);
//
//      cout << "TABU ALGORITHM TIME: "<<tabuAlgorithmTime.count() << endl;


}
