//
//  AdjacencyMatrix.cpp
//  Lab3
//
//  Created by Katelyn Dunn on 10/31/18.
//  Copyright © 2018 Katelyn Dunn. All rights reserved.
//

#include "AdjacencyMatrix.hpp"


AdjacencyMatrix::AdjacencyMatrix(int n)
{
    this->n=n;
    visited = new bool [n];
    adj = new float* [n];
    for (int i = 0; i < n; i++)
    {
        adj[i] = new float [n];
        for(int j = 0; j < n; j++)
        {
            adj[i][j] = 0;
        }
    }
}

void AdjacencyMatrix::addEdge(int start, int end, float weight)
{
    if( start > n || end > n || start < 0 || end < 0)
    {
        cout<<"INVALID\n";
    }
    else
    {
        adj[start-1][end-1] = weight;
    }
}

void AdjacencyMatrix::printAM()
{
    int i,j;
    for(i = 0;i < n;i++)
    {
        for(j = 0; j < n; j++)
            cout<<adj[i][j]<<"  ";
        cout<<endl;
    }
}

int AdjacencyMatrix::size()
{
    return this->n;
}

int AdjacencyMatrix::get(int start, int end)
{
    //cout << start << " --> " << end<<endl;
    return adj[start-1][end-1];
}

//float AdjacencyMatrix::greedy(float start){
//    
//}
