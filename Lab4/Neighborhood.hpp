//
//  Neighborhood.hpp
//  Lab4
//
//  Created by Katelyn Dunn on 11/17/18.
//  Copyright © 2018 Katelyn Dunn. All rights reserved.
//

//#ifndef Neighborhood_hpp
//#define Neighborhood_hpp
#pragma once
#include <vector>
#include <stdio.h>

using namespace std;

class Neighborhood{
public:
    Neighborhood();
    ~Neighborhood();
    Neighborhood(const Neighborhood&);
    Neighborhood& operator=( const Neighborhood& n );
    vector<float> path; //neighborhood
    float distCost; //DISTANCE only cost
    int penalty; //growing penalty
    int swap; //what to put in tabu list
    //void addToPath(float place);
   // float calculatePath();
private:

    
};

//#endif /* Neighborhood_hpp */
