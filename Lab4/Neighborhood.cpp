//
//  Neighborhood.cpp
//  Lab4
//
//  Created by Katelyn Dunn on 11/17/18.
//  Copyright © 2018 Katelyn Dunn. All rights reserved.
//

#include "Neighborhood.hpp"

//void Neighborhood::addToPath(float n){
//    path.push_back(n);
//}

Neighborhood::Neighborhood(){
    distCost = 0; //DISTANCE only cost
    penalty = 0; //growing penalty
    swap = 0;
}

Neighborhood::~Neighborhood(){

}

Neighborhood::Neighborhood(const Neighborhood &n){
    distCost = n.distCost;
    penalty = n.penalty;
    swap = n.swap;
    for (int i = 0; i < n.path.size(); i++)
    {
        path.push_back(n.path[i]);
    }
}

Neighborhood& Neighborhood::operator=( const Neighborhood& n ){
    if(this != &n)
    {
        distCost = n.distCost;
        penalty = n.penalty;
        swap = n.swap;
        path.clear();
        for (int i = 0; i < n.path.size(); i++)
        {
            path.push_back(n.path[i]);
        }
    }
    return *this;
}



