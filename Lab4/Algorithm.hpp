//
//  Algorithm.hpp
//  Lab4
//
//  Created by Katelyn Dunn on 11/17/18.
//  Copyright © 2018 Katelyn Dunn. All rights reserved.
//

#ifndef Algorithm_hpp
#define Algorithm_hpp
#include "AdjacencyMatrix.hpp"
#include "Node.hpp"
#include "Neighborhood.hpp"

#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <cmath>
#include <string>
#include <chrono>

using namespace std;
#include <stdio.h>

class Algorithm{
public:
    AdjacencyMatrix load(string, int);
    virtual vector<float>  search(AdjacencyMatrix adjMat, int numberOfIterations, int size, int startAt) = 0;
    virtual void  calculatePathCost(Neighborhood&, AdjacencyMatrix) = 0;
private:

};
#endif /* Algorithm_hpp */
