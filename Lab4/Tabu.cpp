//
//  Tabu.cpp
//  Lab4
//
//  Created by Katelyn Dunn on 11/17/18.
//  Copyright © 2018 Katelyn Dunn. All rights reserved.
//

#include "Tabu.hpp"

vector<float> Tabu::search(AdjacencyMatrix adjMat, int numberOfIterations, int size, int start){
    //INITAL SOLUTION------------------------------------
    Neighborhood initialSolution;
    for(int i = 1; i <= size; i++ )
    {
        initialSolution.path.push_back(i);
    }
    initialSolution.path.push_back(start);
    
    calculatePathCost(initialSolution, adjMat);

    cout << "STARTING INITIAL SOLUTION PATH: "<< endl;
    for(int i = 0; i < initialSolution.path.size(); i++){
        cout<< initialSolution.path[i];
    }
    cout << endl <<"DISTANCE COST: "<< initialSolution.distCost<<endl<<endl;
    
    
    for(int i = 0; i < initialSolution.path.size(); i++)
    {
        bestPath.push_back(initialSolution.path[i]);
        currMinPath.push_back(initialSolution.path[i]);
    }
    
    bestCost = initialSolution.distCost;
    currMinCost = initialSolution.distCost;

    for(int i = 0; i < numberOfIterations; i++)
    {
        initialSolution = neighborSelection1(initialSolution, adjMat);
       // initialSolution = neighborSelection2(initialSolution, adjMat);
    }
    for(int s = 0; s < initialSolution.path.size(); s++)
    {
        cout<<initialSolution.path[s] << " ";
    }
    cout <<endl<<"COST: "<<initialSolution.distCost << endl;
    return initialSolution.path;
}
//---------------------------------------------------------

void Tabu::calculatePathCost(Neighborhood& n, AdjacencyMatrix adjMat){
    for(int i = 0; i < n.path.size()-1; i++)
    {
        n.distCost += adjMat.get(n.path[i],n.path[i+1]);
    }

}


Neighborhood& Tabu::neighborSelection1(Neighborhood& initialSolution, AdjacencyMatrix& adjMat){
   
    neighbors.clear();
   // tabuList.clear();
    //make new neighborhoods, front switch
    for(int i = 1; i < initialSolution.path.size()-2; i++)
    {
        for(int k = i + 1; k < initialSolution.path.size()-1; k++)
        {
            Neighborhood n;
            n = initialSolution;
            n.distCost = 0;
            
            set<float> swapPairs;
            swapPairs.insert(n.path[i]);
            swapPairs.insert(n.path[k]);
            if (tabuList.find(swapPairs) != tabuList.end())
            {
                if(tabuList.at(swapPairs) > 0)
                {
                    //temp variables to test if fits Aspiration
                    Neighborhood tempNeighbor;
                    tempNeighbor = n;
                    tempNeighbor.distCost = 0;
                    
                    float tempVal;
                    tempVal = tempNeighbor.path[i];
                    tempNeighbor.path[i]= tempNeighbor.path[k];
                    tempNeighbor.path[k] = tempVal;
                    tempNeighbor.penalty += 1;
                    
                    calculatePathCost(tempNeighbor, adjMat);
                    
                    //ASPIRATION CRITERION - BEST EVER
                    //IF IT FITS ASPIRATION: DO IT, current min,
                    //ADD TO TABU??
                    
                    if(tempNeighbor.distCost < bestCost)
                    {
                        currMinPath = tempNeighbor.path;
                        currMinCost = tempNeighbor.distCost;
                        
                        if(neighbors.size() == 1){
                            if((n.distCost + n.penalty)< currMinCost)
                            {
                                currMinCost = n.distCost;
                                currMinPath = n.path;
                                currSwap.clear();
                                currSwap.insert(n.path[i]);
                                currSwap.insert(n.path[k]);
                            }
                        }
                        else if ((n.distCost+n.penalty) < (neighbors[i-1].distCost + n.penalty)) //+penalty
                        { //add penalty to distCost
                            if((n.distCost + n.penalty) < currMinCost)
                            {
                                
                                currMinCost = n.distCost;
                                currMinPath = n.path;
                                currSwap.clear();
                                currSwap.insert(n.path[i]); //
                                currSwap.insert(n.path[k]);
                            }
                        }
                      
                    }
                    //IF IT DOES NOT FIT ASPIRATION: DONT DO THE SWAP
                    
                }
                tabuList.at(swapPairs) -= 1 ;
            }
            else
            {
                
                float temp;
                temp = n.path[i];
                n.path[i]=n.path[k];
                n.path[k] = temp;
                n.penalty += 1;
                
                calculatePathCost(n, adjMat);
                //cout << n.distCost << endl;
                
                neighbors.push_back(n);
                
                //check the cost here - is it the best ever & is it the min (compared to the last ones)
                if(neighbors.size() == 1){
                    if((n.distCost + n.penalty)< currMinCost)
                    {
                        currMinCost = n.distCost;
                        currMinPath = n.path;
                        currSwap.clear();
                        currSwap.insert(n.path[i]);
                        currSwap.insert(n.path[k]);
                    }
                }
                else if ((n.distCost+n.penalty) < (neighbors[i-1].distCost + n.penalty)) //+penalty
                { //add penalty to distCost
                    if((n.distCost + n.penalty) < currMinCost)
                    {
                        currMinCost = n.distCost;
                        currMinPath = n.path;
                        currSwap.clear();
                        currSwap.insert(n.path[i]); //
                        currSwap.insert(n.path[k]);
                    }
                }
            }
            if(currMinCost < bestCost)
            {
                bestPath.clear();
                bestPath = currMinPath;
                bestCost = currMinCost;
            }
            
        }
    }
    
    initialSolution.path = currMinPath;
    initialSolution.distCost = currMinCost;
    tabuList.emplace(currSwap,10);
    
    
    return initialSolution;
}


Neighborhood& Tabu::neighborSelection2(Neighborhood& initialSolution, AdjacencyMatrix& adjMat){

    neighbors.clear();
    //tabuList.clear();

        Neighborhood n;
        n = initialSolution;
        n.distCost = 0;

        set<float> swapPairs;
        int min = initialSolution.path[1];
        int range = (initialSolution.path.size()-1) - min;

        float swap1 =  rand() % range + min;
        float swap2 =  rand() % range + min;

        if (swap1 == swap2)
        {
            swap2 =  rand() % range + min;;
        }
        swapPairs.insert(swap1);
        swapPairs.insert(swap2);

        if (tabuList.find(swapPairs) != tabuList.end())
        {
            if(tabuList.at(swapPairs) > 0)
            {
                //temp variables to test if fits Aspiration
                Neighborhood tempNeighbor;
                tempNeighbor = n;
                tempNeighbor.distCost = 0;

                float tempVal;
                tempVal = tempNeighbor.path[swap1];
                tempNeighbor.path[swap1]= tempNeighbor.path[swap2];
                tempNeighbor.path[swap2] = tempVal;
                tempNeighbor.penalty += 1;

                calculatePathCost(tempNeighbor, adjMat);

                //ASPIRATION CRITERION - BEST EVER
                //IF IT FITS ASPIRATION: DO IT, current min,

                if(tempNeighbor.distCost < bestCost)
                {
                    currMinPath = tempNeighbor.path;
                    currMinCost = tempNeighbor.distCost;
                    
                    if(neighbors.size() == 1){
                        if((n.distCost + n.penalty)< currMinCost)
                        {
                            currMinCost = n.distCost;
                            currMinPath = n.path;
                            currSwap.clear();
                            currSwap.insert(n.path[swap1]);
                            currSwap.insert(n.path[swap2]);
                        }
                    }
                    else if ((n.distCost+n.penalty) < (neighbors[neighbors.size()-1].distCost + n.penalty)) //+penalty
                    { //add penalty to distCost
                        if((n.distCost + n.penalty) < currMinCost)
                        {
                            
                            currMinCost = n.distCost;
                            currMinPath = n.path;
                            currSwap.clear();
                            currSwap.insert(n.path[swap1]); //
                            currSwap.insert(n.path[swap2]);
                        }
                }
                //IF IT DOES NOT FIT ASPIRATION: DONT DO THE SWAP
            }
                tabuList.at(swapPairs) -= 1;
        }
    }
    else
    {
        float temp;
        temp = n.path[swap1];
        n.path[swap1]=n.path[swap2];
        n.path[swap2] = temp;
        n.penalty += 1;

        calculatePathCost(n, adjMat);
        //cout << n.distCost << endl;

        neighbors.push_back(n);

        //check the cost here - is it the best ever & is it the min (compared to the last ones)
        if(neighbors.size() == 1){
            if((n.distCost + n.penalty)< currMinCost)
            {
                currMinCost = n.distCost;
                currMinPath = n.path;
                currSwap.clear();
                currSwap.insert(n.path[swap1]);
                currSwap.insert(n.path[swap2]);
            }
        }
        else if ((n.distCost+n.penalty) < (neighbors[neighbors.size()-1].distCost + n.penalty)) //+penalty
        { //add penalty to distCost
            if((n.distCost + n.penalty) < currMinCost)
            {
                currMinCost = n.distCost;
                currMinPath = n.path;
                currSwap.clear();
                currSwap.insert(n.path[swap1]); //
                currSwap.insert(n.path[swap2]);
            }
        }
    }

//        for (std::map<set<float>,int>::iterator it=tabuList.begin(); it!=tabuList.end(); ++it)
//        {
//            it->second -= 1;
//        }

        if(currMinCost < bestCost)
        {
            bestPath.clear();
            bestPath = currMinPath;
            bestCost = currMinCost;
        }
    
    
    initialSolution.path = currMinPath;
    initialSolution.distCost = currMinCost;
    tabuList.emplace(currSwap,50);

    return initialSolution;
}
