//
//  Dijkstra.cpp
//  CSE3353Lab2
//
//  Created by Katelyn Dunn on 9/10/18.
//  Copyright © 2018 Katelyn Dunn. All rights reserved.
//

#include "Dijkstra.hpp"
vector<int> Dijstra::dijstraSearch(vector<Node>& nodes, int start, int end){
    path.push_back(start); //start is int
    while(start != end)
    {
        if(visitedNodeVals.find(start) == visitedNodeVals.end() || visitedNodeVals.empty())
        {
            visitedNodeVals[start]= currWeight;
            for(int i = 0; i < nodes[(start-1)].destinations.size(); i++)
            {

                if(visitedNodeVals.find(nodes[(start-1)].destinations[i]) == visitedNodeVals.end())
                {
                    path.push_back(nodes[start-1].destinations[i]);
                    //paths.push(path);
                    path.pop_back();
                }
                else
                {
                    continue;
                }
            }
        }
        if (paths.empty())
        {
            path.clear();
            return path;
        }

//        path = paths.top();
//        paths.pop();
        currWeight = nodes[start-1].weightMap[(path.size()-1)];
        start = path[path.size()-1];
        
    }
    return vector<int>(10);

}

double Dijstra::getPathCost(vector<Node> nodes)
{
    int total = 0;
    for(int i = 0; i < path.size(); i++)
    {
        int nodeVal = path[i];
        int next = path[i+1];
        total += nodes[nodeVal - 1].weightMap[next];
    }
    return total;
    
}

void Dijstra::putNode(int,int){
    
}
void Dijstra::pullNode(int,int){
    
}
