//
//  MergeSort.hpp
//  CSE3353Lab1
//
//  Created by Katelyn Dunn on 9/5/18.
//  Copyright © 2018 Katelyn Dunn. All rights reserved.
//

#ifndef MergeSort_hpp
#define MergeSort_hpp
#include <vector>
#include <stdio.h>
#include <vector>
using namespace std;


class MergeSort{
public:
    MergeSort();
    static void sort(vector<int>&);
private:
    static void mergesort(vector<int>&,vector<int>&,vector<int>&);
};
#endif /* MergeSort_hpp */
