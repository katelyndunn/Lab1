//
//  Search.hpp
//  CSE3353Lab2
//
//  Created by Katelyn Dunn on 9/12/18.
//  Copyright © 2018 Katelyn Dunn. All rights reserved.
//

#ifndef Search_hpp
#define Search_hpp

#include <stdio.h>

using namespace std;
#include <stdio.h>
#include <vector>
#include <cmath>
#include <chrono>
#include "Algorithm.hpp"
#include "Node.hpp"
#include "DFS.hpp"
#include "BFS.hpp"

class Algorithm;

class Search: public Algorithm {

private:
    void split(stringstream&);
    vector<Node> nodes;
    
    string s;
    Node n;
    int time = 0;
    int cost = 0;
    int start;
    int end;
    vector<int> results;
    
    void loadWeights(string);
    void loadPosition(string);
    
    int getPathCost(vector<Node>&, vector<int>);
    int getPathDistance(vector<Node>&, vector<int>);

public:
    Search();
    void run() override;
    void load(string) override;
    void save(ofstream&) override;
    void execute(string, int, int);
    void stats() override;
    
};
#endif /* Search_hpp */
