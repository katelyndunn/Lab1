//
//  Node.hpp
//  CSE3353Lab2
//
//  Created by Katelyn Dunn on 9/12/18.
//  Copyright © 2018 Katelyn Dunn. All rights reserved.
//

#ifndef Node_hpp
#define Node_hpp
#include <vector>
#include <map>
using namespace std;
#include <stdio.h>
struct Node{
public: //CHANGE THIS
    int nodeVal;
    int positions[3];
    vector<int> destinations;
    map<int,int> weightMap;
//    void setNode(int);
//    void setNodeDest(int);
//    void setPosition(int,int,int);
//    void setNodeDestWeight(int,int);
//    void clear();
};
#endif /* Node_hpp */

