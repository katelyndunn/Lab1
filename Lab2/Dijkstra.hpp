//
//  Dijkstra.hpp
//  CSE3353Lab2
//
//  Created by Katelyn Dunn on 9/10/18.
//  Copyright © 2018 Katelyn Dunn. All rights reserved.
//

#ifndef Dijkstra_hpp
#define Dijkstra_hpp
#include <vector>
#include <queue>
#include <set>
#include <map>
#include <iostream>
#include "Node.hpp"
using namespace std;

#include <stdio.h>
class Dijstra{
public:
    vector<int> dijstraSearch(vector<Node>&,int, int);
    double getPathCost(vector<Node> node);
    vector<vector<int>> paths;
    map<int,int> visitedNodeVals;
    vector<int> path;
    void putNode(int,int);
    void pullNode(int, int);
    int currWeight = 0;
};
#endif /* Dijkstra_hpp */
