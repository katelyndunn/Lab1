//
//  BFS.hpp
//  CSE3353Lab2
//
//  Created by Katelyn Dunn on 9/10/18.
//  Copyright © 2018 Katelyn Dunn. All rights reserved.
//

#ifndef BFS_hpp
#define BFS_hpp
#include "Node.hpp"
#include <set>
#include <algorithm>
#include <vector>
#include <iostream>
#include <queue>
#include <set>
#include <stdio.h>
using namespace std;

#include <stdio.h>
class BFS{
public:
    vector<int> iBFS(vector<Node>&,int,int); //iterative BFS
    vector<int> rBFS1(vector<Node>&,int,int); //recursive BFS
    vector<int> rBFS2(queue<vector<int>>);
private:
    set<int> visitedNodeVals;
    queue<vector<int>> paths;
    vector<int> path;
    
    queue<vector<int>> rpaths;
};
#endif /* BFS_hpp */
