//
//  Search.cpp
//  CSE3353Lab2
//
//  Created by Katelyn Dunn on 9/12/18.
//  Copyright © 2018 Katelyn Dunn. All rights reserved.
//

#include "Search.hpp"
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>

Search::Search() {
    
}
void Search::run(){
    load("/Users/katelyndunn/Desktop/Fall 2018/Fundamentals of Algorithms (CSE 3353)/CSE3353Lab2/CSE3353Lab2/CSE3353Lab2/SampleGraph/graph.txt");
    //n.clear();
    loadWeights("/Users/katelyndunn/Desktop/Fall 2018/Fundamentals of Algorithms (CSE 3353)/CSE3353Lab2/CSE3353Lab2/CSE3353Lab2/SampleGraph/weights.txt");
    loadPosition("/Users/katelyndunn/Desktop/Fall 2018/Fundamentals of Algorithms (CSE 3353)/CSE3353Lab2/CSE3353Lab2/CSE3353Lab2/SampleGraph/positions.txt");

    //AdjL();
}

void Search::load(string fileName){
    string line;
    ifstream readFile;
    
    readFile.open(fileName);
    if (readFile.is_open())
    {
        
        char* charStar = new char[50];
        while (readFile.getline(charStar,50))
        {
            line = charStar;
            stringstream  ss(line);
            int k;
            ss >> k;
            n.nodeVal=(k);
            
            char comma;
            ss >> comma;
            
            
            while(ss >> k)
            {
                if(ss.peek() == ','){
                    ss.ignore();
                }
                n.destinations.push_back(k);
            }
            nodes.push_back(n);
            //*charStar = NULL;//clear??
            n.destinations.clear();
        }
    }
    else
    {
        std::cout << "Unable to open file: " << fileName;
    }
    loadWeights("/Users/katelyndunn/Desktop/Fall 2018/Fundamentals of Algorithms (CSE 3353)/CSE3353Lab2/CSE3353Lab2/CSE3353Lab2/SampleGraph/weights.txt");
    loadPosition("/Users/katelyndunn/Desktop/Fall 2018/Fundamentals of Algorithms (CSE 3353)/CSE3353Lab2/CSE3353Lab2/CSE3353Lab2/SampleGraph/positions.txt");
}
void Search::loadWeights(string fileName){
    string line;
    ifstream readFile;
    
    readFile.open(fileName);
    if (readFile.is_open())
    {
        char* charStar = new char[50];
        while (readFile.getline(charStar,50))
        {
            line = charStar;
            stringstream  ss(line);
            int nodeValue;
            ss >> nodeValue;
            char comma = 0;
            ss >> comma;
            int childVal = 0;
            ss >> childVal;
            ss >> comma;
            int weight = 0;
            ss >> weight;
            for(int i = 0; i < nodes.size(); i++)
            {
                int n = nodes[i].nodeVal;
                if(n == nodeValue)
                {
                    for(int j = 0; j < nodes.size(); j++)
                    {
                        if (nodes[i].destinations[j] == childVal)
                        {
                            nodes[i].weightMap.insert(std::pair<int,int>(childVal,weight));
                            continue;
                        }
                    }
                }
            }
        }
        n.weightMap.clear();
    }
    else
    {
        std::cout << "Unable to open file: " << fileName;
    }
}

void Search::loadPosition(string fileName){
    string line;
    ifstream readFile;
    
    readFile.open(fileName);
    if (readFile.is_open())
    {
        char* charStar = new char[50];
        while (readFile.getline(charStar,50))
        {
            line = charStar;
            stringstream  ss(line);
            int nodeValue;
            ss >> nodeValue;
            char comma = 0;
            ss >> comma;
            int xVal = 0;
            ss >> xVal;
            ss >> comma;
            int yVal = 0;
            ss >> yVal;
            int zVal = 0;
            ss >> zVal;
            for(int i = 0; i < nodes.size(); i++)
            {
                int n = nodes[i].nodeVal;
                if(n == nodeValue)
                {
                    for(int j = 0; j < nodes.size(); j++)
                    {
                        if (nodes[i].nodeVal == nodeValue)
                        {
                            nodes[i].positions[0]=xVal;
                            nodes[i].positions[1]=yVal;
                            nodes[i].positions[2]=zVal;
                        }
                    }
                }
            }
        }
        //*charStar = NULL;//clear??
    }
    else
    {
        std::cout << "Unable to open file: " << fileName;
    }
}

void Search::execute(string algo , int startp, int endp)
{
    s = algo;
    if (s == "DFSi")
    {
        DFS dfs;
        start = startp;
        end = endp;
        
        //DFS ITERATIVE
        auto start_time = chrono::high_resolution_clock::now();
        results = dfs.iDFS(nodes,start,end);
        auto end_time = chrono::high_resolution_clock::now();
        time= chrono::duration_cast<chrono::microseconds>(end_time - start_time).count();
        cost = getPathCost(nodes, results);
        stats();
        
    }
    if(s =="DFSr")
    {
        DFS dfs;
        start = startp;
        end = endp;
        
        //DFS RECURSION
        auto start_time = chrono::high_resolution_clock::now();
        results = dfs.rDFS1(nodes, start, end);
        auto end_time = chrono::high_resolution_clock::now();
        time= chrono::duration_cast<chrono::microseconds>(end_time - start_time).count();
        getPathCost(nodes, results);
        stats();
        
        
        
    }
    if(s == "BFSi")
    {
        BFS bfs;
        start = startp;
        end = endp;
        
        //BFS ITERATIVE
        auto start_time = chrono::high_resolution_clock::now();
        results = bfs.iBFS(nodes,start,end);
        auto end_time = chrono::high_resolution_clock::now();
        time= chrono::duration_cast<chrono::microseconds>(end_time - start_time).count();
        cost = getPathCost(nodes, results);
        stats();
    }
    
    
    
}
void Search::stats()
{
    cout << "ALGORITHM NAME: " << s <<endl;
    cout << "PATH FROM: "<<start << " TO: "<< end <<endl;
    for(int i = 0; i < results.size(); i++)
    {
        cout<<results[i]<<" ";
    }
    cout << endl;
    cout << "TIME(ms): "<< time<<endl;
    cout << "COST: "<< cost<<endl;
}

int Search::getPathCost(vector<Node>& nodes, vector<int> path){
    int total = 0;
    for(int i = 0; i < path.size(); i++)
    {
        int nodeVal = path[i];
        int next = path[i+1];
        total += nodes[nodeVal - 1].weightMap[next];
    }
    return total;
}

void Search::save(ofstream& outputFile)
{
    
    outputFile.open("/Users/katelyndunn/Desktop/Fall 2018/Fundamentals of Algorithms (CSE 3353)/CSE3353Lab2/CSE3353Lab2/CSE3353Lab2/output.txt");
    
    outputFile << "ALGORITHM NAME: " << s <<endl;
    outputFile<< "PATH FROM: "<<start << " TO: "<< end <<endl;
    for(int i = 0; i < results.size(); i++)
    {
        outputFile<<results[i]<<" ";
    }
    outputFile << endl;
    outputFile << "TIME(ms): "<< time<<endl;
    outputFile << "COST: "<< cost<<endl;
    outputFile << endl;
    
}
int Search::getPathDistance(vector<Node>& nodes, vector<int> path)
{
//    int distance = 0, xVal = 0, yVal = 0, zVal = 0, xVal2 = 0, yVal2 = 0, zVal2 = 0;
//    for (int i = 0; i < path.size()-1; i++)
//    {
//        int nodeVal = path[i];
//        xVal = nodes[nodeVal - 1].positions[0];
//        yVal = nodes[nodeVal - 1].positions[1];
//        zVal = nodes[nodeVal - 1].positions[2];
//
//        int next = path[i+1];
//        xVal2 = nodes[next - 1].positions[0];
//        yVal2 = nodes[next - 1].positions[1];
//        zVal2 = nodes[next - 1].positions[2];
//
//        }
//
//        distance += sqrt(pow((xVal - xVal2), 2) + pow((yVal - yVal2), 2) + pow((zVal - zVal2), 2));
//
//    cout << "distance: "<<distance;
    return 0;
}


















