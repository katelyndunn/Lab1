//
//  BubbleSort.hpp
//  CSE3353Lab1
//
//  Created by Katelyn Dunn on 9/5/18.
//  Copyright © 2018 Katelyn Dunn. All rights reserved.
//

#ifndef BubbleSort_hpp
#define BubbleSort_hpp
#include <vector>
using namespace std;
#include <stdio.h>

class BubbleSort{
public:
    static void sort(vector<int>&);
};
#endif /* BubbleSort_hpp */
