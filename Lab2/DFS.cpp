//
//  DFS.cpp
//  CSE3353Lab2
//
//  Created by Katelyn Dunn on 9/10/18.
//  Copyright © 2018 Katelyn Dunn. All rights reserved.
//

#include "DFS.hpp"
using namespace std;

vector<int> DFS::iDFS(vector<Node>& nodes, int start, int stop){
    path.push_back(start); //start is int
    while(start != stop)
    {
        if(visitedNodeVals.find(start) == visitedNodeVals.end() || visitedNodeVals.empty())
        {
            visitedNodeVals.insert(start);
            for(int i = 0; i < nodes[(start-1)].destinations.size(); i++)
            {
                
                if(visitedNodeVals.find(nodes[(start-1)].destinations[i]) == visitedNodeVals.end())
                {
                    path.push_back(nodes[start-1].destinations[i]);
                    paths.push(path);
                    path.pop_back();
                }
                else
                {
                    continue;
                }
            }
        }
        
        
        if (paths.empty())
        {
            path.clear();
            return path;
        }
        
        path = paths.top();
        paths.pop();
        start = path[path.size()-1];
    }
    return path;
    
}

vector<int> DFS::rDFS1(vector<Node>& nodes, int start, int end){
    vector<int> final;
    int val = start;
    path.clear();
    visitedNodeVals.clear();
    if (visitedNodeVals.find(start) != visitedNodeVals.end() || visitedNodeVals.empty()) //if the vertex is unvisited then visit it
    {
        path.push_back(start);
        visitedNodeVals.insert(start);
    }
    
    return rDFS2(nodes, val, end, start);
}

vector<int> DFS::rDFS2(vector<Node>& nodes ,int val, int end, int start)
{
    if(end == val){
        return path;
    } //fetching the top-most element of the stack //display the dfs
     for(int i = 0; i < nodes[(val-1)].destinations.size(); i++)
            {
                if(visitedNodeVals.find(nodes[(val-1)].destinations[i]) == visitedNodeVals.end())
                {
                    path.push_back(nodes[val-1].destinations[i]);
                    paths.push(path);
                    path.pop_back();
                }
                else
                {
                    continue;
                }
            }
    path = paths.top();
    paths.pop();
    val = path[path.size()-1];
    visitedNodeVals.insert(val);
    rDFS2(nodes, val, end, start);

        return path;
}
