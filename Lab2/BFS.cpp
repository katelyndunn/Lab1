//
//  BFS.cpp
//  CSE3353Lab2
//
//  Created by Katelyn Dunn on 9/10/18.
//  Copyright © 2018 Katelyn Dunn. All rights reserved.
//

#include "BFS.hpp"
vector<int> BFS::iBFS(vector<Node>& nodes, int start, int stop){
    path.push_back(start); //start is int
    while(start != stop)
    {
        if(visitedNodeVals.find(start) == visitedNodeVals.end() || visitedNodeVals.empty())
        {
            visitedNodeVals.insert(start);
            for(int i = 0; i < nodes[(start-1)].destinations.size(); i++)
            {
                
                if(visitedNodeVals.find(nodes[(start-1)].destinations[i]) == visitedNodeVals.end())
                {
                    path.push_back(nodes[start-1].destinations[i]);
                    paths.push(path);
                    path.pop_back();
                }
                else
                {
                    continue;
                }
            }
        }
        
        
        if (paths.empty())
        {
            path.clear();
            return path;
        }
        
        path = paths.back();
        paths.pop();
        start = path[path.size()-1];
    }
    return path;
    
}
//void BFS::rBFS1(vector<int>& nodes,int start,int end){
//    vector<int> startV;
//    startV.push_back(start);
//    rpaths.push(startV);
//    rBFS2(rpaths);
//}
//vector<int> BFS::rBFS2(vector<int> path){
//    if (path.empty())
//    {
//        return path;
//    }
//    rpaths.push(path);
//    path.pop_back();
//    paths.pop();
//    cout << "Node: " << n;
//    for(int i = 0; i < path.size(); i++)
//    {
//        if (nodes[(path[i]-1)].destinaton[i])
//        {
//            path.push_back(nodes[(path[i]-1)].destinaton[i]);
//        }
//
//                breadth_first_recursive(path)
//    }
//    
//    return path;
//}



