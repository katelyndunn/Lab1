//
//  InsertionSort.cpp
//  CSE3353Lab1
//
//  Created by Katelyn Dunn on 9/5/18.
//  Copyright © 2018 Katelyn Dunn. All rights reserved.
//

#include "InsertionSort.hpp"

InsertionSort::InsertionSort() 
{
    
}
void InsertionSort::sort(vector<int>& originalFile)
{
    int a = 0;
    int point = 0;
    int b = 0; 
    for (a = 1; a < originalFile.size(); a++)
    {
        point = originalFile[a];
        b = a-1;
        
        while (b >= 0 && originalFile[b] > point)
        {
            originalFile[b+1] = originalFile[b];
            b = b-1;
        }
        originalFile[b+1] = point;
    }
}

