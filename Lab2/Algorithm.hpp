#ifndef Algorithm_hpp
#define Algorithm_hpp
#include <string>
#include <stdio.h>
#include <vector>
#include <chrono>
#include <ctime>
using namespace std;

class Algorithm
{
public:
    virtual void run()=0;
    virtual void load(string)=0;
    //virtual void execute()=0;
    //virtual void display()=0;
    virtual void stats()=0;
   // virtual void select(string)=0;
    virtual void save(ofstream&)=0;
};

#endif /* Algorithm_hpp */

