//
//  DFS.hpp
//  CSE3353Lab2
//
//  Created by Katelyn Dunn on 9/10/18.
//  Copyright © 2018 Katelyn Dunn. All rights reserved.
//

#ifndef DFS_hpp
#define DFS_hpp
#include "Node.hpp"
#include <set>
#include <algorithm>
#include <vector>
#include <iostream>
#include <stack>
#include <set>
#include <stdio.h>
using namespace std;

class DFS{
public:
    vector<int> iDFS(vector<Node>&,int,int); //iterative DFS
    vector<int> rDFS1(vector<Node>&, int, int); //recursive DFS
    vector<int> rDFS2(vector<Node>&, int,int,int);
    int getPathCost(vector<Node>&, vector<int>);
    
private:
    set<int> visitedNodeVals;
    stack<vector<int>> paths;
    vector<int> path;
};

#endif /* DFS_hpp */
