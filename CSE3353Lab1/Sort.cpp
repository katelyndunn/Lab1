#include "Sort.hpp"
#include <vector>
#include <string>
using namespace std;

#include "Sort.hpp"
#include <iostream>
#include <fstream>
#include <vector>

Sort::Sort() {
    
}
void Sort::run(){
    string sortAlgorithms[] = {"bubble","insertion", "merge"};
    string dataSets[16] = {"/Users/katelyndunn/Desktop/Fall 2018/Fundamentals of Algorithms (CSE 3353)/CSE3353Lab1/CSE3353Lab1/20p10.txt","/Users/katelyndunn/Desktop/Fall 2018/Fundamentals of Algorithms (CSE 3353)/CSE3353Lab1/CSE3353Lab1/20p1000.txt","/Users/katelyndunn/Desktop/Fall 2018/Fundamentals of Algorithms (CSE 3353)/CSE3353Lab1/CSE3353Lab1/20p10000.txt","/Users/katelyndunn/Desktop/Fall 2018/Fundamentals of Algorithms (CSE 3353)/CSE3353Lab1/CSE3353Lab1/20p100000.txt","/Users/katelyndunn/Desktop/Fall 2018/Fundamentals of Algorithms (CSE 3353)/CSE3353Lab1/CSE3353Lab1/random10.txt","/Users/katelyndunn/Desktop/Fall 2018/Fundamentals of Algorithms (CSE 3353)/CSE3353Lab1/CSE3353Lab1/random1000.txt","/Users/katelyndunn/Desktop/Fall 2018/Fundamentals of Algorithms (CSE 3353)/CSE3353Lab1/CSE3353Lab1/random10000.txt","/Users/katelyndunn/Desktop/Fall 2018/Fundamentals of Algorithms (CSE 3353)/CSE3353Lab1/CSE3353Lab1/random100000.txt","/Users/katelyndunn/Desktop/Fall 2018/Fundamentals of Algorithms (CSE 3353)/CSE3353Lab1/CSE3353Lab1/reverse10.txt","/Users/katelyndunn/Desktop/Fall 2018/Fundamentals of Algorithms (CSE 3353)/CSE3353Lab1/CSE3353Lab1/reverse1000.txt","/Users/katelyndunn/Desktop/Fall 2018/Fundamentals of Algorithms (CSE 3353)/CSE3353Lab1/CSE3353Lab1/reverse10000.txt","/Users/katelyndunn/Desktop/Fall 2018/Fundamentals of Algorithms (CSE 3353)/CSE3353Lab1/CSE3353Lab1/reverse100000.txt","/Users/katelyndunn/Desktop/Fall 2018/Fundamentals of Algorithms (CSE 3353)/CSE3353Lab1/CSE3353Lab1/30p10.txt","/Users/katelyndunn/Desktop/Fall 2018/Fundamentals of Algorithms (CSE 3353)/CSE3353Lab1/CSE3353Lab1/30p1000.txt","/Users/katelyndunn/Desktop/Fall 2018/Fundamentals of Algorithms (CSE 3353)/CSE3353Lab1/CSE3353Lab1/30p10000.txt","/Users/katelyndunn/Desktop/Fall 2018/Fundamentals of Algorithms (CSE 3353)/CSE3353Lab1/CSE3353Lab1/30p100000.txt"};
    ofstream outputFile;
    outputFile.open("/Users/katelyndunn/Desktop/Fall 2018/Fundamentals of Algorithms (CSE 3353)/CSE3353Lab1/CSE3353Lab1/output.txt");
    
    for (int i = 0; i < 3; i++)
    {
        select(sortAlgorithms[i]);
        for (int i = 0; i < 16; i++)
        {
            load(dataSets[i]);
            auto start_time = chrono::high_resolution_clock::now();
            execute();
            auto end_time = chrono::high_resolution_clock::now();
            elapsedSeconds = chrono::duration_cast<chrono::seconds>(end_time - start_time).count();
            elapsedMicroSeconds= chrono::duration_cast<chrono::microseconds>(end_time - start_time).count();
            stats();
            //display();
            save(outputFile);
        }
    }
    outputFile.close();
    
}
void Sort::load(string fileName)
{
    originalFile.clear();
    string line;
    ifstream readFile;
    readFile.open(fileName);
    
    if (readFile.is_open())
    {
        while (getline (readFile,line))
        {
            int intLine = stoi(line);
            originalFile.push_back(intLine);
        }
    }
    else
    {
        cout << "Unable to open file: " << fileName;
    }
}
void Sort::execute()
{
    if (chosenAlgorithm == "bubble")
    {
        BubbleSort bs;
        bs.sort(originalFile);
    }
    else if(chosenAlgorithm == "merge")
    {
        MergeSort ms;
        ms.sort(originalFile);
    }
    else if (chosenAlgorithm == "insertion")
    {
        InsertionSort is;
        is.sort(originalFile);
    }
    
}
void Sort::display()
{
    cout << chosenAlgorithm <<endl;
    for (int i = 0; i < originalFile.size(); i++)
    {
        cout << originalFile[i]<<endl;
    }
    cout << endl;
}

void Sort::stats()
{
    cout << "ALGORITHM NAME: " << chosenAlgorithm <<endl;
    if(elapsedSeconds == 0){
        cout << "EXECUTION TIME IN MICROSECONDS:" << elapsedMicroSeconds << endl;
    }
    else{
        cout << "EXECUTION TIME IN SECONDS:" << elapsedSeconds << endl;
    }
    cout << "NUMBER OF RECORDS ANALYZED: "<< originalFile.size() <<endl;
}
void Sort::select(string selectAlgorithmType)
{
    chosenAlgorithm = selectAlgorithmType;
}

void Sort::save(ofstream &outputFile)
{
    outputFile << chosenAlgorithm<<endl;
    for (int i = 0; i < originalFile.size(); i++)
    {
        outputFile << originalFile[i]<<endl;
    }
    outputFile << endl << endl;
}



