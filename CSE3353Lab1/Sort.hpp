//
//  Sort.hpp
//  CSE3353Lab1
//
//  Created by Katelyn Dunn on 9/3/18.
//  Copyright © 2018 Katelyn Dunn. All rights reserved.
//

#ifndef Sort_hpp
#define Sort_hpp
using namespace std;
#include <stdio.h>
#include <vector>
#include "Algorithm.hpp"
#include "BubbleSort.hpp"
#include "MergeSort.hpp"
#include "InsertionSort.hpp"

class Algorithm;

class Sort: public Algorithm {
private:
    double elapsedSeconds;
    double elapsedMicroSeconds;
    string file;
    string chosenAlgorithm;
    vector<int> originalFile;
public:
    Sort();
    void run() override;
    void load(string) override;
    void execute() override;
    void display() override;
    void stats() override;
    void select(string) override;
    void save(ofstream&) override;
};

#endif /* Sort_hpp */
