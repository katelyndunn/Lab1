//
//  main.cpp
//  CSE3353Lab1
//
//  Created by Katelyn Dunn on 8/27/18.
//  Copyright © 2018 Katelyn Dunn. All rights reserved.
//

#include <iostream>
#include "Algorithm.hpp"
#include "Sort.hpp"
using namespace std;
int main(int argc, const char * argv[]) {
    Sort s;
    s.run();
}
