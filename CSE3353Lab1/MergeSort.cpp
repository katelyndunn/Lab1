//
//  MergeSort.cpp
//  CSE3353Lab1
//
//  Created by Katelyn Dunn on 9/5/18.
//  Copyright © 2018 Katelyn Dunn. All rights reserved.
//

#include "MergeSort.hpp"

MergeSort::MergeSort()
{
    
}

void MergeSort::sort(vector<int>& originalFile)
{
    if (originalFile.size() == 1){
        return ;
    }
    
    int middle = (originalFile.size() / 2);
    vector<int> leftvector;
    vector<int> rightvector;
    
    for (int j = 0; j < middle;j++)
        leftvector.push_back(originalFile[j]);
    for (int j = 0; j < (originalFile.size()) - middle; j++)
        rightvector.push_back(originalFile[middle + j]);
    
    sort(leftvector);
    sort(rightvector);
    mergesort(leftvector, rightvector, originalFile);
    
}
void MergeSort::mergesort(vector<int>& left, vector<int>& right, vector<int>& originalFile)
{
    int Lsize = left.size();
    int Rsize = right.size();
    int a = 0;
    int b = 0;
    int c = 0; 
    
    while (b < Lsize && c < Rsize)
    {
        if (left[b] < right[c]) {
            originalFile[a] = left[b];
            b++;
        }
        else {
            originalFile[a] = right[c];
            c++;
        }
        a++;
    }
    while (b < Lsize) {
        originalFile[a] = left[b];
        b++; a++;
    }
    while (c < Rsize) {
        originalFile[a] = right[c];
        c++; a++;
    }
    
}
