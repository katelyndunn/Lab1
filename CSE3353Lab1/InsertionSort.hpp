//
//  InsertionSort.hpp
//  CSE3353Lab1
//
//  Created by Katelyn Dunn on 9/5/18.
//  Copyright © 2018 Katelyn Dunn. All rights reserved.
//

#ifndef InsertionSort_hpp
#define InsertionSort_hpp
#include <vector>
using namespace std;
#include <stdio.h>

class InsertionSort {
public:
    InsertionSort();
    static void sort(vector<int>&);

};

#endif /* InsertionSort_hpp */
