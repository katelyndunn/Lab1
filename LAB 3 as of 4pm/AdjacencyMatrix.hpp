//
//  AdjacencyMatrix.hpp
//  Lab3
//
//  Created by Katelyn Dunn on 10/31/18.
//  Copyright © 2018 Katelyn Dunn. All rights reserved.
//

#ifndef AdjacencyMatrix_hpp
#define AdjacencyMatrix_hpp

#include <iostream>

using namespace std;


#include <stdio.h>

class AdjacencyMatrix{
private:
    int n;
    float ** adj;
    bool* visited;
    
public:
    AdjacencyMatrix(int n);
    void addEdge(int start, int end, float weight);
    void printAM();
    int get(int start, int end);
    int size();
    
};
#endif /* AdjacencyMatrix_hpp */
