//
//  AdjacencyMatrix.cpp
//  Lab3
//
//  Created by Katelyn Dunn on 10/31/18.
//  Copyright © 2018 Katelyn Dunn. All rights reserved.
// based off of https://www.sanfoundry.com/cpp-program-implement-adjacency-matrix/

#include "AdjacencyMatrix.hpp"


AdjacencyMatrix::AdjacencyMatrix(int n)
{
    this->n=n;
    adj = new float* [n];
    for (int i = 0; i < n; i++)
    {
        adj[i] = new float [n];
        for(int j = 0; j < n; j++)
        {
            adj[i][j] = 0;
        }
    }
}

void AdjacencyMatrix::addEdge(int start, int end, float weight)
{
    if( start > n || end > n || start < 0 || end < 0)
    {
        continue;
    }
    else
    {
        adj[start-1][end-1] = weight;
    }
}

void AdjacencyMatrix::printAM()
{
    for(int i = 0;i < n;i++)
    {
        for(int j = 0; j < n; j++)
            cout<<adj[i][j]<<"  "<<endl;
    }
}

int AdjacencyMatrix::size()
{
    return this->n;
}

int AdjacencyMatrix::get(int start, int end)
{
    return adj[start][end];
}
