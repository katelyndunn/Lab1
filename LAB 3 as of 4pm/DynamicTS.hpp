//
//  DynamicTS.hpp
//  Lab3
//
//  Created by Katelyn Dunn on 10/31/18.
//  Copyright © 2018 Katelyn Dunn. All rights reserved.
//

#ifndef DynamicTS_hpp
#define DynamicTS_hpp

#include <stdio.h>

#include "AdjacencyMatrix.hpp"

#include <set>
#include <map>
#include <vector>
#include <iterator>
#include <algorithm>

using namespace std;

class DynamicTS{
private:
    int visited[7] = {0};
    set<int> nodes;
    int currentMin=0;
    vector<set<int>> visitedSets;
    map<std::set<int>,int> costMap[7];
    
public:
    void travelingSalesMan(AdjacencyMatrix adjMat);
    void createPaths(AdjacencyMatrix adjMat,int start, set<int>, int);
};


#endif /* DynamicTS_hpp */

