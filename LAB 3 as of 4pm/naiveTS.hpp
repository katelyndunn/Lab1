//
//  NaiveTS.hpp
//  Lab3
//
//  Created by Katelyn Dunn on 10/25/18.
//  Copyright © 2018 Katelyn Dunn. All rights reserved.
//

#ifndef NaiveTS_hpp
#define NaiveTS_hpp

#include "AdjacencyMatrix.hpp"

#include <stdio.h>
#include <vector>
#include <array>
#include <iostream>
#include <float.h>
using namespace std;

class NaiveTS{
private:
    vector<int> visited;
public:
    float travelingSalesman(AdjacencyMatrix adj, int s);
};

#endif /* NaiveTS_hpp */
